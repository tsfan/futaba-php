<?php
// 全般設定---------------------------------------------------------------------
require_once 'bbsinfo.php';

define('POST_LIST', 'postlist.log');
define('POST_BEGIN', 'post_begin');
define('POST_END', 'post_end');
define('POST_COUNT', 'post_count');
define('POST_POINT', 'post_point');

main();
exit;

function main()
{

    switch ($_GET['pt']) {
        case '0':    //小記事があるもの
            $listtype = 0;
            $listfile = POST_LIST . $listtype;
            break;
        case '1':    //pointあり
            $listtype = 1;
            $listfile = POST_LIST . $listtype;
            break;
        default:    //その他
            $listfile = POST_LIST;
    }

    if (!file_exists($listfile) || filemtime(TREEFILE) > filemtime($listfile)) {
        //ファイルがない or 記事一覧が古い
        $tree = file(TREEFILE);
        $line = file(LOGFILE);
        
        mb_internal_encoding('UTF-8');
        $treedat     = makePostNoList($tree);
        //var_dump($treedat);
        $postlist     = makePostList($treedat, $line);
        $html         = makeListHtml($postlist, $listtype);
        
        $fp = fopen($listfile, "w");
        flock($fp, LOCK_EX);    //ロック
        fwrite($fp, $html);
        flock($fp, LOCK_UN);    //ロック解除
        fclose($fp);
        chmod($listfile, 0666);
    } else {
        //記事一覧HTMLのほうが新しい
        $html = file_get_contents($listfile);
    }
    echo $html;
}
/**
 * TREEFILEから1レス目と最終レス番号を取得
 *    最終レス番号が存在しない場合はNULL
 *
 * @param 配列 $tree
 */
function makePostNoList($tree)
{

    //投票結果を取得
    if (USE_VOTE == 1 && file_exists(VOTE_SCRIPT)) {
        include_once VOTE_SCRIPT;
        if (function_exists("GetCountData")) {
            $countdata=GetCountData();
        }
    }

    $i=0;
    foreach ($tree as $posttree) {
        $postlist = explode(',', $posttree);

        $treedat[$i][POST_POINT] = 0;
        foreach ($postlist as $postno) {
            $postno = trim($postno);
            if (isset($countdata[$postno])) {
                $treedat[$i][POST_POINT] = $treedat[$i][POST_POINT] + $countdata[$postno];
            }
        }

        $treedat[$i][POST_BEGIN]     =  trim(array_shift($postlist));
        $treedat[$i][POST_END]         =  trim(array_pop($postlist));
        $treedat[$i][POST_COUNT]    =  count($postlist);

        $i++;
    }
    return $treedat;
}
/**
 *    makePostNoListで作成したリストをもとに記事の内容を取得する
 */
function makePostList($treedat, $line)
{

    //とりあえず$lineを分解する
    foreach ($line as $linedat) {
        list($no,$now,$name,,$sub,$com,,,,$ext,,,$time,) = explode(",", $linedat);
        $postdat[$no]['no']        = $no;            //記事番号
        $postdat[$no]['time']    = $time;        //最初の投稿時間
        $postdat[$no]['now']    = $now;            //最後の投稿時間
        $postdat[$no]['name']    = $name;        //作者名
        $postdat[$no]['title']    = $sub;            //タイトル
        $postdat[$no]['com']    = strip_tags($com);    //本文
        $postdat[$no]['file']    = $time.$ext;    //画像ファイル名
    }
    //var_dump($postdat);
    
    //投稿一覧を作成
    $i=0;
    $newtime = (time() - (3600 * 24)) * 1000;    //1日以内 & マイクロ秒対応
    //echo $newtime;
    
    foreach ($treedat as $treepostnum) {
        //ひとまず代入
        $postlist[$i]['no']             = $postdat[$treepostnum[POST_BEGIN]]['no'];
        $postlist[$i]['name']             = $postdat[$treepostnum[POST_BEGIN]]['name'];
        $postlist[$i]['title']             = $postdat[$treepostnum[POST_BEGIN]]['title'];
        $postlist[$i]['file']             = $postdat[$treepostnum[POST_BEGIN]]['file'];
        $postlist[$i]['BEGIN_com']         = $postdat[$treepostnum[POST_BEGIN]]['com'];
        $postlist[$i]['BEGIN_time']     = $postdat[$treepostnum[POST_BEGIN]]['time'];
        $postlist[$i]['point_int']        = $treepostnum[POST_POINT];
        if ($treepostnum[POST_END]<>'') {
            $postlist[$i]['END_com']         = $postdat[$treepostnum[POST_END]]['com'];

            $endTime     = strtr(
                $postdat[$treepostnum[POST_END]]['now'],
                array('(月)'=>' ', '(火)'=>' ', '(水)'=>' ', '(木)'=>' ', '(金)'=>' ', '(土)'=>' ', '(日)'=>' ')
            ) . ':00';
            $endTime     = strptime($endTime, '%y/%m/%d %T');
            $endTime    = mktime(
                $endTime['tm_hour'],
                $endTime['tm_min'],
                $endTime['tm_sec'],
                $endTime['tm_mon']+1,
                $endTime['tm_mday'],
                $endTime['tm_year']
            );
            $postlist[$i]['END_time'] = strval($endTime). '000';
        }
        $postlist[$i][POST_COUNT]         = $treepostnum[POST_COUNT];
        
        //*---- 各種書き換え ----*
        //名前がDEF_NAME以外なら強調
        if (NONE_NAME !== $postlist[$i]['name']) {
            $postlist[$i]['name'] = "<b>" . $postlist[$i]['name'] . "</b>";
        }
        
        //タイトルがDEF_SUB以外なら協調。それ以外は無題に
        if (NONE_SUB !== $postlist[$i]['title']) {
            $postlist[$i]['title'] = "<b>" . $postlist[$i]['title'] . "</b>";
        } else {
            $postlist[$i]['title'] = "無題";
        }
        
        //画像へのリンク
        if ($postlist[$i]['file']<>'') {
            $postlist[$i]['file'] = '<a href="' . IMG_DIR . $postlist[$i]['file'] .'">' . "[画像]</a>" ;
        }
        
        //本文を整形
        if (NONE_COM !== $postlist[$i]['BEGIN_com']) {
            $postlist[$i]['BEGIN_com'] = mb_strimwidth($postlist[$i]['BEGIN_com'], 0, 40, '...');
        } else {
            $postlist[$i]['BEGIN_com'] = '';
        }
        
        if (!array_key_exists('END_com', $postlist)) {
            $postlist[$i]['END_com'] = mb_strimwidth($postlist[$i]['END_com'], 0, 40, '...');
        }
        
        //時間でNEW
        if ($postlist[$i]['BEGIN_time'] > $newtime) {
            $postlist[$i]['BEGIN_com'] = '<font color="red">[new]</font> ' . $postlist[$i]['BEGIN_com'];
        }
        
        //echo $postlist[$i]['END_time'] . '/'. $newtime. "<br />\n";
        if ($postlist[$i]['END_time'] > $newtime) {
            $postlist[$i]['END_com'] = '<font color="red">[new]</font> ' . $postlist[$i]['END_com'];
        }

        if ($postlist[$i]['point_int'] >= BORDER_POINT_PARENT) {
            $postlist[$i]['point'] = '<font color="#E06030">(' . $postlist[$i]['point_int'] . ' pts.)</font>';
        }
        
        $i++;
    }
    return $postlist;
}
/**
 * 記事一覧をHTML出力
 *
 * @param unknown_type $postlist
 */
function makeListHtml($postlist, $listtype)
{

    head($html);
    
    $html .= '[<a href="'.PHP_SELF2.'">掲示板に戻る</a>]';
    $html .= "<br />\n";
    $html .= "<br />\n";
        
    $i = 1;
    $writeWord = '%d.%s/<a href="%s">%s(%d)</a>/ %s %s';
    foreach ($postlist as $post) {
        $linehtml = sprintf($writeWord, $i, $post['name'], PHP_SELF.'?res='.$post['no'], $post['title'], $post['post_count']+1, $post['file'], $post['BEGIN_com']);
        if ($post['END_com']<>'') {
            $linehtml .= '<font color="#9acd32"> // </font>' . $post['END_com'];
        }
        if ($post['point']<>'') {
            $linehtml .= ' ' . $post['point'];
        }
        $linehtml .= "<br />\n";

        switch ($listtype) {
            case '0':
                if (isset($post['point']) || $post['BEGIN_com']<>'' || $post['END_com']<>'') {
                    $html .= $linehtml;
                    $i++;
                }
                break;
            case '1':
                if ($post['point']<>'') {
                    $html .= $linehtml;
                    $i++;
                }
                break;
            default:
                $html .= $linehtml;
                $i++;
        }
    }
    
    foot($html);
    
    return $html;
    //var_dump($postlist);
}

function head(&$dat)
{
    $dat.='<html><head>
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=UTF-8">
<meta name="Berry" content="no">
<meta HTTP-EQUIV="pragma" CONTENT="no-cache">
<STYLE TYPE="text/css">
<!--
body,tr,td,th { font-size:12pt }
a:hover { color:#DD0000; }
span { font-size:20pt }
small { font-size:10pt }
-->
</STYLE>
<title>'.TITLE.'</title>
<script language="JavaScript"><!--
function l(e){var P=getCookie("pwdc"),N=getCookie("namec"),i;with(document){for(i=0;i<forms.length;i++){if(forms[i].pwd)with(forms[i]){pwd.value=P;}if(forms[i].name)with(forms[i]){name.value=N;}}}};onload=l;function getCookie(key, tmp1, tmp2, xx1, xx2, xx3) {tmp1 = " " + document.cookie + ";";xx1 = xx2 = 0;len = tmp1.length;  while (xx1 < len) {xx2 = tmp1.indexOf(";", xx1);tmp2 = tmp1.substring(xx1 + 1, xx2);xx3 = tmp2.indexOf("=");if (tmp2.substring(0, xx3) == key) {return(unescape(tmp2.substring(xx3 + 1, xx2 - xx1 - 1)));}xx1 = xx2 + 1;}return("");}
//--></script>
</head>
<body bgcolor="#FFFFEE" text="#800000" link="#0000EE" vlink="#0000EE">
<p align=right>
[<a href="'.HOME.'" target="_top">ホーム</a>]
[<a href="'.PHP_SELF.'?mode=admin">管理用</a>]
<p align=center>
<font color="#800000" size=5>
<b><SPAN>'.TITLE.'</SPAN></b></font>
<hr width="90%" size=1>
';
}
function foot(&$dat)
{
    $dat.='
<center>
<small><!-- GazouBBS v3.0 --><!-- ふたば改0.8 -->
- <a href="http://php.s3.to" target=_top>GazouBBS</a> + <a href="http://www.2chan.net/" target=_top>futaba</a>- customized version
</small>
</center>
</body></html>';
}
