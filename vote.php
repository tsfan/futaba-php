<?php
/*-----------------------------------------------
 *	投票用php
 *-----------------------------------------------*/
require_once "bbsinfo.php";        // 管理者情報等をインクルード

if (basename($_SERVER['SCRIPT_NAME'])==='vote.php') {
    //チェック
    if (count($_GET)<1) {
        voteError("パラメータが不正です。");
    }

    if (voteProxy_connect('80') == 1) {
        voteError("ＥＲＲＯＲ！　公開ＰＲＯＸＹ規制中！！(80)");
    } elseif (voteProxy_connect('8080') == 1) {
        voteError("ＥＲＲＯＲ！　公開ＰＲＯＸＹ規制中！！(8080)");
    }

    //処理分岐
    foreach ($_GET as $key => $GetValue) {
        switch ($key) {
            case 'GJ':
                if (VOTE_CHECKHOST) {
                    $HostRemo     = gethostbyaddr($_SERVER["REMOTE_ADDR"]);
                    $HostError     = CheckHost($HostRemo, $isp);
                    if ($HostError) {
                        voteError($HostRemo."からは投票できません(".$HostError.")");
                    }
                }
                if (is_numeric($GetValue)) {
                    CheckDbFile();
                    CheckTreeFile($GetValue);
                    SetCountData($GetValue, 1, POINT_GJ);
                } else {
                    voteError("パラメータが不正です。");
                }
                break 2;
            case 'BOO':
                if (VOTE_CHECKHOST) {
                    $HostRemo     = gethostbyaddr($_SERVER["REMOTE_ADDR"]);
                    $HostError     = CheckHost($HostRemo, $isp);
                    if ($HostError) {
                        voteError($HostRemo."からは投票できません(".$HostError.")");
                    }
                }
                if (is_numeric($GetValue)) {
                    CheckDbFile();
                    CheckTreeFile($GetValue);
                    SetCountData($GetValue, -1, POINT_BOO);
                } else {
                    voteError("パラメータが不正です。");
                }
                break 2;
            case '_update0805':
                UpdateList();
                break 2;
            case '_output0805':
                OutputList();
                break 2;
            default:
                voteError("パラメータが不正です。");
                break 2;
        }
    }
}

//DBチェック＆作成
function CheckDbFile()
{
    if (is_file(DBCOUNT)) {
        return;
    }

    if (! $db = new PDO("sqlite:".DBCOUNT)) {
        die("DB Connection Failed.");
    }
    $sql = "CREATE TABLE counttab(resno INTEGER, datetime INTEGER, pt INTEGER, ip TEXT, ua TEXT);";
    if ($db->exec($sql)===0) {
        voteError("DB作成に失敗しました。");
    }

    $db = null;
}
//Treeファイルチェック
function CheckTreeFile($resno)
{

    $tree = file(TREEFILE);
    $find = false;
    if ($resno) {
        $counttree=count($tree);
        for ($i = 0; $i<$counttree; $i++) {
            foreach (explode(",", rtrim($tree[$i])) as $posno) {
                if ($posno==$resno) {
                    $find=true;
                    break 2;
                } //レス先検索
            }
        }
        if (!$find) {
            voteError("該当記事がみつかりません");
        }
    }
}
//値取得
function GetCountData()
{
    if (! $db = new PDO("sqlite:".DBCOUNT)) {
        //voteError
        return array();
    }

    //$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

    $sql = "SELECT resno, SUM(pt) FROM counttab "
    . " GROUP BY resno"
    . " ORDER BY resno;";
    //echo $sql . "<br />";
    $stmt = $db->query($sql);

    $i = 0;
    $pointdat = array();
    if($stmt !== FALSE) {
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
            $pointdat[$row['resno']] = $row["SUM(pt)"];
        }
    }
    $db = null;

    return $pointdat;
}

/* カウントデータ登録 */
function SetCountData($resno, $pt, $votept)
{
    if (! $db = new PDO("sqlite:".DBCOUNT)) {
        $mes = "DBのOPENに失敗しました。<br />" ;
        $mes = "しばらく待ってから再実行するか、管理者へ連絡してください。" ;
        voteError($mes);
    }

    $sql = "SELECT * FROM counttab "
    . "WHERE ip = " . $db->quote($_SERVER["REMOTE_ADDR"])
    . " ORDER BY datetime;";

    //echo $sql . "<br />";
    $stmt=$db->query($sql);

    if ($stmt !== FALSE) {
        $i = 0;
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
            //print_r($row);
            if ($resno == $row['resno']) {
                //投票済み
                $com = "すでに投票済みです。<br />";
                $com .= "<small>(前回投票時間:" . date("Y/m/d H:i", $row['datetime']) . "ごろ(サーバー時間))</small>";
                voteError($com);
            }
            if ($row['datetime'] >= time() - INTERVAL_SEC) {
                //指定秒数以内
                $com = INTERVAL_SEC . "秒以内の連続投票はできません。<br />";
                $com .= "<small>(前回投票時間:" . date("Y/m/d H:i", $row['datetime']) . "ごろ(サーバー時間))</small>";
                voteError($com);
            }
            if ($row['datetime'] >= time() - 86400) {
                if ($row['pt'] > 0) {
                    $i = $i + POINT_GJ;
                } else {
                    $i = $i + POINT_BOO;
                }
            }
        }
        if ($i + $votept > DAY_MAX) {
            $com = "使用済み投票数は" . $i . "回です。24時間以内の最大投票数は" . DAY_MAX . "回までです。<BR>" .
                "(GJ 1回につき " . POINT_GJ . "投票分／Boo 1回につき " . POINT_BOO . "投票分を消費します)";
            voteError($com);
        }

        $sql = "INSERT INTO counttab(resno, datetime, pt, ip, ua) "
            . "VALUES(" . $db->quote($resno) . ","
            . $db->quote(time()) . ","
            . $db->quote($pt) . ","
            . $db->quote($_SERVER["REMOTE_ADDR"]) . ","
            . $db->quote($_SERVER["HTTP_USER_AGENT"]) . ");";

        if ($db->exec($sql) === 0) {
            $mes = "DB更新に失敗しました:INSERT<br />";
            $mes = "しばらく待ってから再実行するか、管理者へ連絡してください。";
            voteError($mes);
        } else {
            $com = "投票を受け付けました。(" . ($i + 1) . "/" . DAY_MAX . ")";
            voteError($com);
        }
    }

    $db = null;
}
// カウントデータを削除
function DeleteCountData($dellist)
{

    if (! $db = new PDO("sqlite:".DBCOUNT)) {
        $mes = "DBのOPENに失敗しました。<br />" ;
        $mes = "しばらく待ってから再実行するか、管理者へ連絡してください。" ;
        voteError($mes);
    }
    file_put_contents("test.txt", $dellist);

    $delno=explode(",", rtrim($dellist));

    $sql = 'DELETE FROM counttab WHERE resno=:resno;';
    $sth = $db->prepare($sql);

    foreach ($delno as $resno) {
        //print_r($resno);
        $sth->bindParam(':resno', trim($resno), PDO::PARAM_INT);
    }
    $sth->execute();
    //print_r($sth);
}

/* Host Check */
function CheckHost($remo, &$isp)
{
    // http://ken.2ch.net/source.cgi?file=/ken_carrier.cgi    20120804

    //$time_start = microtime(true);

    if (preg_match('/\d+\.\d+\.\d+\.\d+$/', $remo)) {
        $isp="アラビア";
        return 1000;
    }
    //if($remo !~ /\./', $remo))                                                                    {$isp="逆引き異常 アラビア";                        return 1000;}
    //echo '01:',microtime(true) - $time_start,"<br/>";

    if (preg_match('/\.jp-(d|h|t|k|r|s|n|q|c)\.ne\.jp$/', $remo)) {
        $isp="SBガラケー";
        return 101;
    }
    if (preg_match('/\.panda-world\.ne\.jp$/', $remo)) {
        $isp="SB iPhone/iPad(Smile.World APN)";
        return 102;
    }
    if (preg_match('/\.openmobile\.ne\.jp$/', $remo)) {
        $isp="SB Android系";
        return 103;
    }
    if (preg_match('/\.ezweb\.ne\.jp$/', $remo)) {
        $isp="auガラケー";
        return 110;
    }
    if (preg_match('/\.au-net\.ne\.jp$/', $remo)) {
        $isp="auスマフォ(3G,WiMAX共通)";
        return 111;
    }
    if (preg_match('/\.docomo\.ne\.jp$/', $remo)) {
        $isp="docomoガラケー";
        return 120;
    }
    if (preg_match('/\.mopera\.net$/', $remo)) {
        $isp="docomo mopera";
        return 121;
    }
    if (preg_match('/\.spmode\.ne\.jp$/', $remo)) {
        $isp="docomo spmode";
        return 122;
    }

    if (preg_match('/\.uqwimax\.jp$/', $remo)) {
        $isp="UQ WiMAX";
        return 200;
    }
    if (preg_match('/\.uqw\.ppp\.infoweb\.ne\.jp$/', $remo)) {
        $isp="@Nifty WiMAX";
        return 201;
    }
    if (preg_match('/^UQ.+tky\.mesh\.ad\.jp$/', $remo)) {
        $isp="BIGLOBE WiMAX";
        return 202;
    }
    if (preg_match('/\.wmaxuq\d+\.ap\.so-net\.ne\.jp$/', $remo)) {
        $isp="So-net WiMAX";
        return 203;
    }
    if (preg_match('/^wmxp[\d-]+\.kualnet\.jp$/', $remo)) {
        $isp="kualnet WiMAX";
        return 204;
    }
    if (preg_match('/\.wimax\.auone-net\.jp$/', $remo)) {
        $isp="au one WiMAX";
        return 205;
    }
    if (preg_match('/\.wmx\.home\.ne\.jp$/', $remo)) {
        $isp="J:COM WiMAX";
        return 206;
    }

    if (preg_match('/\.e-mobile\.ne\.jp$/', $remo)) {
        $isp="本家e-mobile.ne.jp";
        return 300;
    }
    if (preg_match('/\.emobile\.ad\.jp$/', $remo)) {
        $isp="本家emobile.ad.jp";
        return 301;
    }
    if (preg_match('/\.em\.ppp\.infoweb\.ne\.jp$/', $remo)) {
        $isp="infoweb e-mobile";
        return 303;
    }
    if (preg_match('/\.iem\.hi-ho\.ne\.jp$/', $remo)) {
        $isp="hi-ho e-mobile";
        return 304;
    }

    if (preg_match('/\.bmobile\.ne\.jp$/', $remo)) {
        $isp="b-mobile";
        return 20;
    }
    if (preg_match('/\.ppp\.prin\.ne\.jp$/', $remo)) {
        $isp="WillCOM";
        return 21;
    }
    
    if (preg_match('/\.fomant\d+\.ap\.so-net\.ne\.jp$/', $remo)) {
        $isp="sonet FOMAデータ通信";
        return 31;
    }
    if (preg_match('/\.vmobile\.jp$/', $remo)) {
        $isp="IIJmio";
        return 32;
    }

    $isp="";
    return 0;
}

//リストを更新
function UpdateList()
{

    if (! $db = new PDO("sqlite:".DBCOUNT)) {
        $mes = "DBのOPENに失敗しました。<br />" ;
        $mes = "しばらく待ってから再実行するか、管理者へ連絡してください。" ;
        voteError($mes);
    }

    $nowtime = time();

    $remohost = array();
    $list = array();

    CalcListPoint($db, $nowtime, $remohost, $list);
    CheckListTable($db);

    //print_r($remohost);
    //print_r($list);

    InsertListTable($db, $nowtime, $remohost, $list);

    $db = null;

    voteError("更新完了:".$nowtime);
}

//投票数計算
function CalcListPoint(&$db, $nowtime, &$remohost, &$list)
{

    $sql = "SELECT * FROM counttab WHERE datetime > " . ($nowtime-(86400*14)) . ";";
    //echo $sql,"<br/>";
    $stmt=$db->query($sql);

    if ($stmt !== FALSE) {

        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
            if (!isset($list[$row['ip']])) {
                $isp = "";
                $list[$row['ip']] = array();
                $list[$row['ip']]['ip'] = $row['ip'];
                $list[$row['ip']]['host'] = gethostbyaddr($row['ip']);
                $list[$row['ip']]['htype'] = CheckHost($list[$row['ip']]['host'], $isp);
                $list[$row['ip']]['pt000'] = 0;
                $list[$row['ip']]['plus000'] = 0;
                $list[$row['ip']]['minus000'] = 0;
                $list[$row['ip']]['pt144'] = 0;
                $list[$row['ip']]['plus144'] = 0;
                $list[$row['ip']]['minus144'] = 0;
                $list[$row['ip']]['pt072'] = 0;
                $list[$row['ip']]['plus072'] = 0;
                $list[$row['ip']]['minus072'] = 0;
                $list[$row['ip']]['pt024'] = 0;
                $list[$row['ip']]['plus024'] = 0;
                $list[$row['ip']]['minus024'] = 0;
            }

            if (!isset($remohost[$list[$row['ip']]['htype']])) {
                $remohost[$list[$row['ip']]['htype']] = array();
                $remohost[$list[$row['ip']]['htype']]['htype'] = $list[$row['ip']]['htype'];
                $remohost[$list[$row['ip']]['htype']]['isp'] = $isp;
                $remohost[$list[$row['ip']]['htype']]['h000'] = 0;
                $remohost[$list[$row['ip']]['htype']]['h144'] = 0;
                $remohost[$list[$row['ip']]['htype']]['h072'] = 0;
                $remohost[$list[$row['ip']]['htype']]['h024'] = 0;
            }

            if ($row['pt'] > 0) {
                $list[$row['ip']]['plus000'] += $row['pt'];
            } else {
                $list[$row['ip']]['minus000'] += $row['pt'];
            }
            $list[$row['ip']]['pt000'] += abs($row['pt']);
            $remohost[$list[$row['ip']]['htype']]['h000']++;

            if ($row['datetime'] > $nowtime - (86400 * 7)) {
                if ($row['pt'] > 0) {
                    $list[$row['ip']]['plus144'] += $row['pt'];
                } else {
                    $list[$row['ip']]['minus144'] += $row['pt'];
                }
                $list[$row['ip']]['pt144'] += abs($row['pt']);
                $remohost[$list[$row['ip']]['htype']]['h144']++;
            }

            if ($row['datetime'] > $nowtime - (86400 * 3)) {
                if ($row['pt'] > 0) {
                    $list[$row['ip']]['plus072'] += $row['pt'];
                } else {
                    $list[$row['ip']]['minus072'] += $row['pt'];
                }
                $list[$row['ip']]['pt072'] += abs($row['pt']);
                $remohost[$list[$row['ip']]['htype']]['h072']++;
            }

            if ($row['datetime'] > $nowtime - (86400 * 1)) {
                if ($row['pt'] > 0) {
                    $list[$row['ip']]['plus024'] += $row['pt'];
                } else {
                    $list[$row['ip']]['minus024'] += $row['pt'];
                }
                $list[$row['ip']]['pt024'] += abs($row['pt']);
                $remohost[$list[$row['ip']]['htype']]['h024']++;
            }
        }
    }

}

//テーブルの削除&作成
function CheckListTable($db)
{
    $db->beginTransaction();

    $db->exec("DROP TABLE [list_htype];");
    $sql = "CREATE TABLE [list_htype] ([htype] VARCHAR(10),"
                    ." [isp] VARCHAR(100),"
                    ." [datatime] VARCHAR(20),"
                    ." [h000] INTEGER DEFAULT '0',"
                    ." [h144] INTEGER DEFAULT '0',"
                    ." [h072] INTEGER DEFAULT '0',"
                    ." [h024] INTEGER DEFAULT '0',"
                    ." PRIMARY KEY(htype)"
                    ." );";
    $db->exec($sql);

    $db->exec("DROP TABLE [list_host];");
    $sql = "CREATE TABLE [list_host] ([ip] VARCHAR(20) NOT NULL UNIQUE,"
                    ." [host] VARCHAR(255),"
                    ." [htype] VARCHAR(10),"
                    ." [datatime] VARCHAR(20),"
                    ." [pt000] INTEGER DEFAULT '0',"
                    ." [plus000] INTEGER DEFAULT '0',"
                    ." [minus000] INTEGER DEFAULT '0',"
                    ." [pt144] INTEGER DEFAULT '0',"
                    ." [plus144] INTEGER DEFAULT '0',"
                    ." [minus144] INTEGER DEFAULT '0',"
                    ." [pt072] INTEGER DEFAULT '0',"
                    ." [plus072] INTEGER DEFAULT '0',"
                    ." [minus072] INTEGER DEFAULT '0',"
                    ." [pt024] INTEGER DEFAULT '0',"
                    ." [plus024] INTEGER DEFAULT '0',"
                    ." [minus024] INTEGER DEFAULT '0',"
                    ." PRIMARY KEY(ip)"
                    ." );";
    $db->exec($sql);

    $db->commit();
}

//リストデータの登録
function InsertListTable(&$db, $nowtime, &$remohost, &$list)
{
    $db->beginTransaction();

    $sql = "INSERT INTO [list_htype] (htype, isp, datatime, h000, h144, h072, h024) "
                                                    . "VALUES (?, ?, ?, ?, ?, ?, ?)";
    $stmt = $db->prepare($sql);
    foreach ($remohost as $row) {
        $flag = $stmt->execute(
            array($row['htype'],$row['isp'], $nowtime,
                                                                    $row['h000'],$row['h144'],$row['h072'],$row['h024'])
        );
    }

    $sql = "INSERT INTO [list_host] (ip, host, htype, datatime, "
                                                                    ."pt000, plus000, minus000, pt144, plus144, minus144, "
                                                                    ."pt072, plus072, minus072, pt024, plus024, minus024) "
                                                    . "VALUES (?, ?, ?, ?,"
                                                                        ."?, ?, ?, ?, ?, ?, "
                                                                        ."?, ?, ?, ?, ?, ? )";
    $stmt = $db->prepare($sql);
    foreach ($list as $row) {
        //print_r($row);
        $flag = $stmt->execute(
            array($row['ip'], $row['host'], $row['htype'], $nowtime,
                                                                    $row['pt000'], $row['plus000'], $row['minus000'],
                                                                    $row['pt144'], $row['plus144'], $row['minus144'],
                                                                    $row['pt072'], $row['plus072'], $row['minus072'],
                                                                    $row['pt024'], $row['plus024'], $row['minus024'])
        );
    }

    $db->commit();
}

//リスト出力
function OutputList()
{
    if (! $db = new PDO("sqlite:".DBCOUNT)) {
        $mes = "DBのOPENに失敗しました。<br />" ;
        $mes = "しばらく待ってから再実行するか、管理者へ連絡してください。" ;
        voteError($mes);
    }

    $msg="";
    votehead($msg);

    $msg.="<H3>	【モバイル種別】</H3>";
    $sql = "SELECT * FROM list_htype ORDER BY h000 DESC;";
    //echo $sql,"<br/>";
    $stmt=$db->query($sql);

    if ($stmt !== FALSE) {

        $msg .= "<table border=1>"
            . "<tr><th>htype</th><th>isp</th><th>h000</th><th>h144</th><th>h072</th><th>h024</th>\r\n";
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
            $msg .= "<tr><td>" . $row['htype'] . "</td><td>" . $row['isp'] . "</td>"
                . "<td>" . $row['h000'] . "</td><td>" . $row['h144'] . "</td><td>" . $row['h072'] . "</td><td>" . $row['h024'] . "</td>\r\n";
        }
        $msg .= "</table>";
        $msg .= "<br /><br /><br />\r\n";
    }

    $msg.="<H3>【ホスト一覧】</H3>";
    $sql = "SELECT * FROM list_host ORDER BY pt000 DESC;";
    //echo $sql,"<br/>";
    $stmt=$db->query($sql);
    if ($stmt !== FALSE) {
        $msg .= "<table border=1>"
            . "<tr><th>ip</th><th>host</th><th>htype</th>"
            . "<th>pt000</th><th>plus000</th><th>minus000</th>"
            . "<th>pt144</th><th>plus144</th><th>minus144</th>"
            . "<th>pt072</th><th>plus072</th><th>minus072</th>"
            . "<th>pt024</th><th>plus024</th><th>minus024</th></tr>\r\n";

        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
            //print_r($row);
            $msg .= "<tr><td>" . $row['ip'] . "</td><td>" . $row['host'] . "</td><td>" . $row['htype'] . "</td>"
                . "<td>" . $row['pt000'] . "</td><td>" . $row['plus000'] . "</td><td><font color='red'><B>" . $row['minus000'] . "</B></font></td>"
                . "<td>" . $row['pt144'] . "</td><td>" . $row['plus144'] . "</td><td><font color='red'><B>" . $row['minus144'] . "</B></font></td>"
                . "<td>" . $row['pt072'] . "</td><td>" . $row['plus072'] . "</td><td><font color='red'><B>" . $row['minus072'] . "</B></font></td>"
                . "<td>" . $row['pt024'] . "</td><td>" . $row['plus024'] . "</td><td><font color='red'><B>" . $row['minus024'] . "</B></font></td></tr>\r\n";
        }
        $msg .= "</table>";
    }

    echo $msg,"</body></html>";
}

/* ヘッダ */
function votehead(&$dat)
{
    $dat.='<html><head>
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=UTF-8">
<meta HTTP-EQUIV="pragma" CONTENT="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<META NAME="ROBOTS" CONTENT="NOINDEX,NOFOLLOW,NOARCHIVE"> 
<STYLE TYPE="text/css">
<!--
body,tr,td,th { font-size:12pt }
a:hover { color:#DD0000; }
span { font-size:20pt }
small { font-size:10pt }
-->
</STYLE>
<title>'.TITLE.'</title>
</head>
<body bgcolor="#FFFFEE" text="#800000" link="#0000EE" vlink="#0000EE">
<p align=right>
[<a href="'.HOME.'" target="_top">ホーム</a>]
[<a href="'.PHP_SELF.'?mode=admin">管理用</a>]
<p align=center>
<font color="#800000" size=5>
<b><SPAN>'.TITLE.'</SPAN></b></font>
<hr width="90%" size=1>
';
}

/* エラー画面 */
function voteError($mes)
{
    global $com;
    // テキストの改行タグ挿入
    $com = str_replace("\r\n", "\n", $com);
    $com = str_replace("\r", "\n", $com);
    if ($com!="" && substr_count($com, "\n")>0) {
        $com = nl2br($com);
    }

    votehead($dat);
    echo $dat;
    echo "<br><br><hr size=1><br><br>
        <center><TABLE><TR><TD>$com</TD></TR></TABLE></center><br>
        <center><font color=red size=5><b>$mes<br><br><a href=".PHP_SELF2.">リロード</a></b></font></center>
        <br><br><hr size=1>";
    die("</body></html>");
}

/* proxyチェック */
function voteProxy_connect($port)
{
    $a="";
    $b="";
    $fp = @fsockopen($_SERVER["REMOTE_ADDR"], $port, $a, $b, 2);
    if (!$fp) {
        return 0;
    } else {
        return 1;
    }
}
