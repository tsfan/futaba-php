<?php
define("LOGFILE", 'img0000.log');		//ログファイル名
define("TREEFILE", 'tree0000.log');		//ログファイル名
define("HOSTFILE", 'host0000.lst');     // 晒しホストの記録ファイル
define("IMG_DIR", 'src/');		//画像保存ディレクトリ。futaba.phpから見て
define("THUMB_DIR",'thumb/');		//サムネイル保存ディレクトリ
define("TITLE", 'ＴＳＦ画像掲示板（テスト板ε）');		//タイトル（<title>とTOP）
define("HOME",  '/main.html');			//「ホーム」へのリンク
define("MAX_KB", '500');			//投稿容量制限 KB（phpの設定により2Mまで
define("MAX_W",  '200');			//投稿サイズ幅（これ以上はwidthを縮小
define("MAX_H",  '200');			//投稿サイズ高さ
define("PAGE_DEF", '10');			//一ページに表示する記事
define("LOG_MAX",  '1600');		//ログ最大行数
define("DEL_ALATE_RATE", 0.60);		//ログ削除警告 表示レート
define("ADMIN_PASS", 'admin#password');	//管理者パス
define("RE_COL", '789922');               //＞が付いた時の色
define("PHP_SELF", 'futaba.php');	//このスクリプト名
define("PHP_SELF2", 'futaba.html');	//入り口ファイル名
define("PHP_EXT", '.html');		//1ページ以降の拡張子
define("RENZOKU", '1');			//連続投稿秒数
define("RENZOKU2", '1');		//画像連続投稿秒数
define("MAX_RES", '300');		//強制sageレス数
define("USE_THUMB", 1);		//サムネイルを作る する:1 しない:0
define("PROXY_CHECK", 0);		//proxyの書込みを制限する y:1 n:0
define("DISP_ID", 2);		//IDを表示する 強制:2 する:1 しない:0
define("BR_CHECK", 72);		//改行を抑制する行数 しない:0
define("IDSEED", '99999');		//idの種
define("RESIMG", 0);		//レスに画像を貼る:1 貼らない:0

/* 追加設定 */
define("TEXT_MAX", 1800);		//最大
define("FILESIZE", 2048000);		//読み込みファイルバッファサイズ 初期値:1000000 1MBくらい
define("COMMENT_COL", '442299');        //コメント行＝#で始まる行の色
define("NONE_NAME", "きよひこ");						//未記入時の名前
define("NONE_COM", "ｷﾀ━━━━(ﾟ∀ﾟ)━━━━ｯ!!");		//未記入時のコメント
define("NONE_SUB", "無題");								//未記入時のタイトル
define("USE_DELFORM", 1);		//削除申請 有効:1 無効:0
define("DELFORM_URL", '');		//削除申請フォームリンク
define("WATCHISP_FILE", "watchIsp0000.txt");    //監視対象ISPファイル
define("USE_MARKDOWN", true);   //Markdown記法の許可
define("SET_MARKDOWN", false);   //Markdown記法の初期設定

//スパム対策
define("ANTISPAM", '0');		//スパム対策 する:1 しない:0
define("ASFIELD_TEXT", '書き込み時、ここを空欄にしてください');		//スパム対策欄に表示するテキスト

//人気投票機能
define("USE_VOTE", 1);		//人気投票機能 有効:1 無効:0
define("VOTE_SCRIPT", "./vote.php");	//投票用php
define("DBCOUNT", "count0000.db");	//カウント用DB
define("INTERVAL_SEC", 90);		//再投稿間隔
define("DAY_MAX", 10);			//1日の最大投票数
define("POINT_GJ", 1);			//投票GJの消費投票数
define("POINT_BOO", 5);			//投票Booの消費投票数
define("BORDER_POINT_PARENT", 5);	//	親記事ポイント数
define("BORDER_POINT_CHILD", 3);	//	子記事ポイント数
define("BORDER_POINT_HIDDEN", -11);	//	非表示ポイント数
define("VOTE_CHECKHOST", 1);	//ホストチェック 有効:1 無効:0

//投稿欄注意書きの追記事項
$addinfotext = '<LI>information</LI>';
?>
